from settings import GITLAB_BASE_URL as GITLAB_URL, LIST_PROJECTS, PROJECT_VARIABLES
from request_base_client import Reqs

req = Reqs()


def list_projects(search_name: bool = True, page: int = 1, log: bool = True) -> list[
    dict]:
    search_str = input(' > Project name contains:') if search_name else ""
    resp = req.get(f"{GITLAB_URL}/{LIST_PROJECTS}?search={search_str}&page={page}")
    if log:
        print("Projects:")
        [print(f" · {r['id']} - {r['name_with_namespace']}") for r in resp]
    return resp


def get_all_project_ids() -> list[int]:
    page = 1
    projects = list_projects(search_name=False, page=page, log=False)
    page_projects = []
    while page_projects or page == 1:
        page += 1
        print(f"Processing page {page} {len(page_projects)}", end="\r")
        page_projects = list_projects(search_name=False, page=page, log=False)
        projects += page_projects
    return [r["id"] for r in projects]


def get_project_variables_page(project_id: str, page: int):
    results = req.get(
        f"{GITLAB_URL}/{PROJECT_VARIABLES.format(project_id=project_id)}?page={page}"
    )
    return results


def get_all_project_variables(project_id: str, page: int = 1):
    page_variables = get_project_variables_page(project_id, page)
    results = page_variables
    while page_variables:
        page += 1
        page_variables = get_project_variables_page(project_id, page)
        results.extend(page_variables)
    return results

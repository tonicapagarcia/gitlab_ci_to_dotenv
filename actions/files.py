import os

from actions.projects import get_all_project_variables
from actions.variables import process_project_variables


def generate_env_file(project_id: str, env_name: str, env_variables: dict):
    env_content = [f"{k}={v}" for k, v in env_variables.items()]
    try:
        os.mkdir(project_id)
    except FileExistsError:
        ...
    with open(f"{project_id}/{env_name}.env", "w") as f:
        f.write("\n".join(env_content))


def generate_env_by_project_id():
    project_id = input(" > Project ID:")
    if project_variables := get_all_project_variables(project_id):
        envs_by_scope = process_project_variables(project_variables)
        for env_name in envs_by_scope.keys():
            if env_name != "*":
                variables = {**envs_by_scope[env_name], **envs_by_scope["*"]}
                generate_env_file(project_id, env_name, variables)
        print("     env generated")
        return project_variables
    print(f" ¡Env variables not found for project {project_id}!")
    return None
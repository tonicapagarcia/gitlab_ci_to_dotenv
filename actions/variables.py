from typing import List

from actions.projects import get_all_project_ids, get_project_variables_page, \
    get_all_project_variables


def process_project_variables(variables: List[dict]) -> dict:
    envs_by_scope = {}
    for v in variables:
        scope = v["environment_scope"]
        if not envs_by_scope.get(scope):
            envs_by_scope[scope] = {}
        envs_by_scope[scope][v["key"]] = v["value"]
    return envs_by_scope


def search_variables() -> List[tuple]:
    search_value = input("Search value:")
    proj_ids = get_all_project_ids()
    variables = []
    for id in proj_ids:
        filtered = []
        print(f"Processing project {id} variables", end="\r")
        for x in get_all_project_variables(project_id=str(id)):
            if x == "message":
                print(f"Error getting variables for project {str(id)}")
                continue
            if x and search_value in x["value"].lower():
                filtered.append(
                    ("Project", str(id), "Variable", x["key"], ":", x["value"])
                )
                print(f"Reading variables for project {id}", end="\r")
        variables.extend(filtered)
    return variables

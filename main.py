from actions.files import generate_env_by_project_id
from actions.projects import list_projects, get_all_project_ids
from actions.variables import search_variables


ACTION_MAP = {
    "0": exit,
    "1": list_projects,
    "2": generate_env_by_project_id,
    "3": get_all_project_ids,
    "4": search_variables
}


def gitlab_env_manager():
    options = '\n\t'.join(
        [f"[{k}] {getattr(v, '__name__', 'exit')}" for k, v in ACTION_MAP.items()]
    )
    first_message = f""" > What do you want to do? \n\t{options} \n\t > Option: """
    action_key = input(first_message)
    result = ACTION_MAP.get(action_key)()
    print(result)
    gitlab_env_manager()


if __name__ == '__main__':
    gitlab_env_manager()

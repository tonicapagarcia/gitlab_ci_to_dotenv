# Generate env files from Gitlab CI Variables

Generate .env files by gitlab CI stage.

Before execute main.py you must add .env file like this:
```
GITLAB_BASE_URL=
GITLAB_TOKEN=
```

Now you are able to execute main.py:
```
 > What do you want to do? 
	[0] exit
	[1] list_projects
	[2] generate_env_by_project_id
	[3] get_all_project_ids
	[4] search_variables 
	 > Option: 
```

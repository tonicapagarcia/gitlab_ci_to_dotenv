import os
from dotenv import load_dotenv

load_dotenv()


GITLAB_BASE_URL = os.environ.get("GITLAB_BASE_URL", "https://gitlab.com/api/v4")
GITLAB_TOKEN = os.environ["GITLAB_TOKEN"]

LIST_GROUPS = "groups"
LIST_PROJECTS = "projects"
PROJECT_VARIABLES = "projects/{project_id}/variables"

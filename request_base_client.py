import requests

from settings import GITLAB_TOKEN


class Reqs:
    def get(self, url: str, headers: dict = {}):
        try:
            headers.update({"PRIVATE-TOKEN": GITLAB_TOKEN})
            r = requests.get(url, headers=headers)
            return r.json()
        except Exception as e:
            print("Unexpected exception")
            return []
